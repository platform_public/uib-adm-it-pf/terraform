module "node" {
  source = "git@github.com:TorLdre/tf-nrec-node.git?ref=mods"

  name      = "gitlab_runners"
  node_name = "p3-gl-runner-"
  # Enable this to create and update fqdn in NREC
#  zone_name         = "whatever.uib.no"
#  domain            = "jalla.whatever.uib.no"
  region            = "bgo"
  node_count        = 3
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["2001:700:200::/48"]
  allow_ssh_from_v4 = ["129.177.0.0/16"]
  network           = "IPv6"
  flavor            = "m1.small"
  image_name        = "GOLD Rocky Linux 8"
  image_user        = "rocky"
  volume_size       = 30
}
